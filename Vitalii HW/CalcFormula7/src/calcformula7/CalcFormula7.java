
package calcformula7;

import java.util.Scanner;

public class CalcFormula7 {


    public static void main(String[] args) {
        System.out.println("Вычисление площади правильного треугольника ");
        System.out.println();
        System.out.println("Введите сторону треугольника ");
        Scanner myInput1 = new Scanner ( System.in  );
        double a = myInput1.nextDouble();
        double S = Math.pow(a, 2)*Math.sqrt(3)/2;
        System.out.println("Площадь ромба равна " + S);
    }
    
}
