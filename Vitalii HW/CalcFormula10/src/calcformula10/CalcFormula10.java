
package calcformula10;

import java.util.Scanner;

public class CalcFormula10 {

    static double askUser(String question){ 
        System.out.println(question);
        return new Scanner(System.in).nextDouble();
    }
    public static void main(String[] args) {
        System.out.println("Вычисление площади треугольника по трем сторонам");
        System.out.println();
        double a=askUser("Ведите сторону а>0");
        double b=askUser("Ведите сторону b>0");
        double c=askUser("Ведите сторону c>0");
        double S=Math.sqrt(((a+b+c)/2)*(((a+b+c)/2)-a)*(((a+b+c)/2)-b)*(((a+b+c)/2)-c));
        System.out.println("Площадь треугольника равна "+S);
    }
    
}
