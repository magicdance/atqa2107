
package calcformula4;

import java.util.Scanner;

public class CalcFormula4 {


    public static void main(String[] args) {
        System.out.println("Вычисление длины окружности ");
        System.out.println();
        System.out.println("Введите радиус окружности ");
        Scanner myInput1 = new Scanner ( System.in  );
        double r = myInput1.nextDouble();
        double L = Math.PI*r*2;
        System.out.println("Длина окружности равна " + L);
    }
    
}
