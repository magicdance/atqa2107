
package calcif20;

import java.util.Scanner;

public class CalcIf20 {


    public static void main(String[] args) {
       System.out.println("Проверка трех чисел на положительность");
        System.out.println();
        System.out.println("Введите число A");
        Scanner myInput1 = new Scanner ( System.in  );
        int a = myInput1.nextInt();
        
        int z = a%10;
        int y = (a/10)%10;
        int x = (a/100)%10;
        
        boolean isPositive = checkIsPositive(x,y,z);
        System.out.println(isPositive);
    }
    public static boolean checkIsPositive(int x, int y, int z)
    {
        return (x < y) && (y < z);
    }
}
