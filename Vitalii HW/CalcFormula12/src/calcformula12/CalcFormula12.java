
package calcformula12;

import java.util.Scanner;

public class CalcFormula12 {
    static double askUser(String question){ 
        System.out.println(question);
        return new Scanner(System.in).nextDouble();
    }

    public static void main(String[] args) {
        System.out.println("Вычисление площади треугольника по стороне и опущенной на нее высоте ");
        System.out.println();
        double a=askUser("Ведите сторону а>0");
        double h=askUser("Ведите высоту h>0");
        double S=a*h/2;
        System.out.println("Площадь треугольника равна "+S);
    }
    
}
