
package calcformula1;

import java.util.Scanner;

public class CalcFormula1 {

   
    public static void main(String[] args) {
        System.out.println("Вычисление расстояния между двумя точками декартовой плоскости");
        System.out.println();
        System.out.println("Введите координату первой точки x1");
        Scanner myInput1 = new Scanner ( System.in  );
        double x1 = myInput1.nextDouble();
        System.out.println("Введите координату первой точки y1");
        Scanner myInput2 = new Scanner ( System.in  );
        double y1 = myInput2.nextDouble();
        System.out.println("Введите координату второй точки x2");
        Scanner myInput3 = new Scanner ( System.in  );
        double x2 = myInput3.nextDouble();
        System.out.println("Введите координату второй точки y2");
        Scanner myInput4 = new Scanner ( System.in  );
        double y2 = myInput4.nextDouble();
        double d = Math.sqrt(Math.pow((x1-x2),2)+ Math.pow((y2-y1),2));
        System.out.println("Расстояние между двумя точками декартовой плоскости равно " + d);
    }
    
}
