
package calcformula11;

import java.util.Scanner;

public class CalcFormula11 {
    static double askUser(String question){ 
        System.out.println(question);
        return new Scanner(System.in).nextDouble();
    }
    public static void main(String[] args) {
        System.out.println("Вычисление площади треугольника по сторонам и углу между ними");
        System.out.println();
        double a=askUser("Ведите сторону а>0");
        double b=askUser("Ведите сторону b>0");
        double alfa=askUser("Ведите угол между сторонами alfa>0");
        double S=((a+b)/2)*Math.sin(Math.toRadians(alfa));
        System.out.println("Площадь треугольника равна "+S);
    }
    
}
