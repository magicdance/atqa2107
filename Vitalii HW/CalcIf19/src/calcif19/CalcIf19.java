
package calcif19;

import java.util.Scanner;

public class CalcIf19 {


    public static void main(String[] args) {
       System.out.println("Проверка трех чисел на положительность");
        System.out.println();
        System.out.println("Введите число A");
        Scanner myInput1 = new Scanner ( System.in  );
        int a = myInput1.nextInt();
        System.out.println("Введите число B");
        Scanner myInput2 = new Scanner ( System.in  );
        int b = myInput2.nextInt();
        System.out.println("Введите число С");
        Scanner myInput3 = new Scanner ( System.in  );
        int c = myInput3.nextInt();
        
        boolean isPositive = checkIsPositive(a, b, c);
        System.out.println(isPositive);
    }
    public static boolean checkIsPositive(int a, int b, int c)
    {
        return (a > 0) && (b < 0) && (a+b == 0) || (a > 0) && (c < 0) && (a+c == 0) || (b > 0) && (c < 0) && (b+c == 0) ||
               (a < 0) && (b > 0) && (a+b == 0) || (a < 0) && (c > 0) && (a+c == 0) || (b < 0) && (c > 0) && (b+c == 0);
    }
}
