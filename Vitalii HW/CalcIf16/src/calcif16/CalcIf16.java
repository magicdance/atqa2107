
package calcif16;

import java.util.Scanner;

public class CalcIf16 {


    public static void main(String[] args) {
        System.out.println("Проверка трех чисел на положительность");
        System.out.println();
        System.out.println("Введите число A");
        Scanner myInput1 = new Scanner ( System.in  );
        int a = myInput1.nextInt();
        
        boolean isPositive = checkIsPositive(a);
        System.out.println(isPositive);
    }
    public static boolean checkIsPositive(int a)
    {
        return ( a>9 && a < 100 && a%2==0);
    }
}
