
package calcformula9;

import java.util.Scanner;

public class CalcFormula9 {

    static double askUser(String question){ 
        System.out.println(question);
        return new Scanner(System.in).nextDouble();
    }
    public static void main(String[] args) {
        System.out.println("Вычисление площади треугольника по известным трем сторонам и радиусу описанной окружности   ");
        System.out.println();
        double a=askUser("Ведите сторону а>0");
        double b=askUser("Ведите сторону b>0");
        double c=askUser("Ведите сторону c>0");
        double R=askUser("Введите радиус описанной окружности R>0 ");
        double S= a*b*c/(4*R);
        System.out.println("Площадь треугольника равна "+S);
    }
    
}
