package employeeexample;


public class Cleaner implements IEmployee {
    
    public int salary;
    public Cleaner(int salary)
    {
        this.salary= salary;
    }
    
    @Override
    public double calcSalary()
    {
        return salary;
    }
    
    @Override
    public String getTitle() 
    {
        return "Уборщик";
    }
}
