package employeeexample;


public class Programmer  implements IEmployee{
    
    int hour;
    int rate;
    public Programmer(int hour, int rate) 
    {
        this.hour= hour;
        this.rate= rate;
    }
    
    double salary;
    @Override
    public double calcSalary()
    {
        return salary=hour*rate;
    }
    
    @Override
    public String getTitle() 
    {
        return "Программист";
    }
}
