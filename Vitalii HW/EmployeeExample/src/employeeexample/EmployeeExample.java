package employeeexample;

import java.util.ArrayList;
import java.util.List;

public class EmployeeExample {


    public static void main(String[] args) { 
        
        Cleaner cl= new Cleaner(1000);
        Programmer pr= new Programmer(160, 16);
        Supervisor sp= new Supervisor(2000, 1000);
        
        List <IEmployee> person= new ArrayList<>();
        
        person.add(cl);
        person.add(pr);
        person.add(sp);
        
        for(int i= 0; i< person.size(); i++)
        {
            double S= person.get(i).calcSalary();
            System.out.println(person.get(i).getTitle());
            //System.out.println(person.get(i).toString());
            System.out.println("Зарплата= "+S);
        }
    }
    
}
