package employeeexample;


public class Supervisor implements IEmployee {
    
    int bonus;
    int rate;
    public Supervisor(int rate , int bonus)
    {
        this.rate= rate;
        this.bonus= bonus;
    }
    double salary;
    @Override
    public double calcSalary() 
    {
        return salary=rate+bonus;
    }

    @Override
    public String getTitle() 
    {
        return "Руководитель";
    }
    
    
}
