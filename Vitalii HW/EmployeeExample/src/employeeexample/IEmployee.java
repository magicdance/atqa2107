package employeeexample;


public interface IEmployee {
    public double calcSalary();
    public String getTitle();
    
}
