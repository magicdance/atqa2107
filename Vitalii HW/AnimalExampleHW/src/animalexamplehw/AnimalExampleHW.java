
package animalexamplehw;


public class AnimalExampleHW {


    public static void main(String[] args) {
       
       MyPet cat= new MyPet("Tom", 1, 18);
       //кот Том, самец, возраст 18месяцев=полтора года
       System.out.println(cat);
       
       Man ivan1= new Man("Иван", "Петров", 1, 12*18);
       System.out.println(ivan1);
       
       Person gmail= new Person("Иван", "Петров", 1, 12*18,"+380503252525" ,"programm@gmail.com" );
       System.out.println(gmail);  
       
       Employee forman= new Employee("Иван", "Петров", 1, 12*18,"+380503252525" ,
                                     "programm@gmail.com", "Forman", "#11223", 1000);
       System.out.println(forman);
    }
    
}
