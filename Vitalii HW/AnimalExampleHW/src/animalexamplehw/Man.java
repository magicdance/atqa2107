package animalexamplehw;

import Animal.Animal;


public class Man extends Animal {

    @Override
    public String toString() {
        return "firstName= " + firstName
             + "lastName= " + lastName
             +" age (year)= "+this.getAge()/12
             +" sex= "+this.getSex();
    }
    private String firstName;
    private String lastName;
    Man(String fName, String lName, int sex, int age)
    {
        super(sex, age);
        this.firstName= fName;
        this.lastName= lName;        
    }
    
}
