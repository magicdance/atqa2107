package animalexamplehw;


public class Person extends Man {

    @Override
    public String toString() {
        return "Person{" +super.toString()+ " email=" + email + ", phone=" + phone + '}'
                ;
    }
    private String email;
    private String phone;
    Person(String fname, String lname, int sex, int age,
           String  phone, String email)
    {
        super(fname, lname, sex, age);
        this.email= email;
        this.phone= phone;
    }
    
}
