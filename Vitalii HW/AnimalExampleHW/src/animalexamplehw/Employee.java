package animalexamplehw;


public class Employee extends Person {

    @Override
    public String toString() {
        return "Employee " +super.toString()+ " position= " + position + 
                " personalNumber= " + personalNumber + ", salary= " + salary;
    }
    
    private String position;
    private String personalNumber;
    private int salary;
    Employee (String fname, String lname, int sex, int age,
               String  phone, String email, String position, 
               String personalnumber, int salary)
    {
        super(fname, lname, sex, age, phone, email);
        this.position= position;
        this.personalNumber= personalnumber;
        this.salary= salary;
    }
            
            
}
