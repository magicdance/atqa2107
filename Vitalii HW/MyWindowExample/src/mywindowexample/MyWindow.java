package mywindowexample;

import java.awt.FlowLayout;
import javax.swing.*;

public class MyWindow extends JFrame {
    //Lable= тэги <p> <h1> <span>
    private JLabel lblMessage;
    //Button= тэги input c type="button"
    private JButton btnAction;
    private JTextField txtInput;
    MyWindow()
    {
        System.out.println("call MyWindow()");
        this.setSize(350, 75);
        this.setTitle("Первое окно");
        lblMessage= new JLabel("Привет мир");
        btnAction= new JButton("Click me!");
        txtInput= new JTextField("Это я Вася.");
        this.setLayout(new FlowLayout() );
        
        this.add(lblMessage);
        this.add(txtInput);
        this.add(btnAction);
        
        this.setVisible(true);
        
    }
    
}
