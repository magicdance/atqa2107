package mathshapes;

import java.util.ArrayList;
import java.util.List;


public class MathShapes {


    public static void main(String[] args) {
        //Создать иерархию наследования квадрат, прямоугольник, треугольник
        //связать эти понятия наследованием
        
        //в будущем может появиться круг (и точка ? )
        //нужно уметь вычислять площадь и периметр фигуры
         
        System.out.println("Hello world MathShapes");
        
        Square sq= new Square(3);
//        double sqArea= sq.calcArea();
//        double sqPerimetr= sq.calcPerimetr();
//        System.out.println("Квадрат"+sq);
//        System.out.println("S= "+sqArea+" P= "+sqPerimetr);
        
        Rectangle rec= new Rectangle(3, 4);
//        double recArea= rec.calcArea();
//        double recPerimetr= rec.calcPerimetr();
//        System.out.println("Прямоугольник"+rec);
//        System.out.println("S= "+recArea+" P= "+recPerimetr);
        
        Triangle trg= new Triangle(3, 4, 5);
//        double s= trg.calcArea();
//        double p= trg.calcPerimetr();
//        System.out.println("Треугольник"+trg);
//        System.out.println("S= "+s+" P= "+p);
        
        Сircle crl= new Сircle(3, 4, 5, 1);
        
        Point pnt= new Point(3, 4, 5, 1, 0);
         
        List <IMathShape> shapes= new ArrayList<>();
        shapes.add(sq);
        shapes.add(rec);
        shapes.add(trg);
        shapes.add(crl);
        shapes.add(pnt);
        for(int i= 0; i< shapes.size(); i++)
        {
            double P= shapes.get(i).calcPerimetr();
            double S= shapes.get(i).calcArea();
            System.out.println(shapes.get(i).getMyClassName());
            System.out.println(shapes.get(i).toString());
            System.out.println("S= "+S+" P= "+P);
        }
    }
}
