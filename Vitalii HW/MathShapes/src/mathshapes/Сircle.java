package mathshapes;


public class Сircle extends Triangle implements IMathShape {
    
    private int r;
    public Сircle(int a, int b, int c, int r) 
    {
        super(a, b, c);
        this.setR(r);
    }

    public int getR() 
    {
        return r;
    }

    public void setR(int r) 
    {
        this.r = r;
    }
    

    @Override
    public double calcArea()
    {
        double s = Math.PI*this.getR()*this.getR();
        return s;
    }
     @Override
    public double calcPerimetr()
    {
        return 2*Math.PI*this.getR();
    }
    @Override
    public String toString() 
    {
        return " r = " + this.getR();
    }
     @Override
    public String getMyClassName() 
    {
        return "Круг";
    }
}
