package mathshapes;


public interface IMathShape {
    public double calcArea();
    public double calcPerimetr();
    public String getMyClassName();
}
