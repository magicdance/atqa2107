package mathshapes;


public class Point extends Сircle implements IMathShape {
    
    private int z;
    public Point(int a, int b, int c, int r, int z) 
    {
        super(a, b, c, r);
        this.setZ(z);
    }

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }

    @Override
    public String toString() {
        return "z=" + z;
    }
    
    @Override
    public double calcArea()
    {
        return 0*this.getZ();
    }
     @Override
    public double calcPerimetr()
    {
        return 0*this.getZ();
    }
    
    @Override
    public String getMyClassName() 
    {
        return "Точка";
    }
}
