package mathshapes;


public class Square implements IMathShape{
    private int a;
    public Square(int a)
    {
        this.a= a;
    }
    
    public int getA()
    {
        return this.a;
    }
    
    public void setA()
    {
        this.a= a;
    }
    
    @Override
    public String toString() {
        return " a= " + a;
    }
    
    public double calcArea()
    {
        return this.getA()*this.getA();
    }
    
    public double calcPerimetr()
    {
        return this.getA()*4;
    }

    @Override
    public String getMyClassName() 
    {
        return "Квадрат";
    }
}
