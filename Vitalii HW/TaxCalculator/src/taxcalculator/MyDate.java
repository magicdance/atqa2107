public class MyDate {
    
    private int day;
    private String month;
    private int year;

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    @Override
    public String toString() {
        return "MyDate{" + "day=" + day + ", month=" + month + ", year=" + year + '}';
    }
    
    //конструкто = правило создания
    //объекта в начальном безопасном состоянии
    //имя конструктора = имя функции = имя класс

    public MyDate(int day, String month, int year) {
        this.day = day;
        this.month = month;
        this.year = year;
    }
  
}