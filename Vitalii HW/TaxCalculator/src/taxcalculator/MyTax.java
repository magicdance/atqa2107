
package taxcalculator;


public class MyTax {

    public MyTax(double salary) {
        this.pn = salary*0.18;
        this.pension = salary*0.22;
        this.vn = salary*0.015;
    }

    @Override
    public String toString() {
        return "MyTax{" + "pn=" + pn + ", pension=" + pension + ", vn=" + vn + '}';
    }
    private double pn;
    private double pension;
    private double vn;
}
