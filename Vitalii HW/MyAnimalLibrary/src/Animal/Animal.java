package Animal;


public class Animal {

    public int getSex() {
        return sex;
    }
    private int sex;
    private int age;//в месяцах

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Animal{" + "sex=" + sex + ", age=" + age + '}';
    }

    public Animal(int sex, int age) {
        this.sex = sex;
        this.age = age;
    }
    
    
}
